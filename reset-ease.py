# Copyright Zachery Gyurkovitz 2018 MIT License -- see LICENSE.md or https://opensource.org/licenses/MIT

from aqt import mw
from aqt.utils import showInfo
from aqt.qt import *

import json

class ModifyEase:
    def __init__(self, mw):
        self.menuAction = QAction("Reset Ease On All Cards", mw)
        mw.connect(self.menuAction, SIGNAL("triggered()"), self.resetEase)
        mw.form.menuTools.addAction(self.menuAction)

    def generateDeckEaseMap(self):
        """Creates a Dictionary of decks to their initial ease factors"""
        decks = json.loads(mw.col.db.scalar("select decks from col"))
        confs = json.loads(mw.col.db.scalar("select dconf from col"))
        confeasemap = dict()
        deckeasemap = dict()
        for confkey, conf in confs.iteritems():    
            confeasemap[conf['id']] = conf['new']['initialFactor']
            
        for deckkey, deck in decks.iteritems():
            deckeasemap[deck['id']] = confeasemap[deck['conf']]
        return deckeasemap

    def resetEase(self):
        deckeasemap = self.generateDeckEaseMap()
        for key, val in deckeasemap.iteritems():
            mw.col.db.execute("update cards set factor = ? where did = ?", val, key)
        showInfo("The ease has been reset.")

if __name__ != "__main__":
    # Save a reference to the toolkit onto the mw, preventing garbage collection of PyQT objects
    if mw: mw.modifyease = ModifyEase(mw)
else:
    print "This is a plugin for the Anki Spaced Repition learning system and cannot be run directly."
    print "Please download Anki2 from <http://ankisrs.net/>"
